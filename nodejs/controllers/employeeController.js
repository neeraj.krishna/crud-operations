const express = require('express')
var router = express.Router();
var nodemailer = require('nodemailer');
var ObjectId = require('mongoose').Types.ObjectId
var {Employee} = require('../models/employee')
router.get('/',(req,res) =>{
    Employee.find((err,docs) =>{
        if(!err){
            res.send(docs)
        }
        else{
            console.log("error in getting the data "+ JSON.stringify(err,undefined,2 ))
        }
    })
})
router.get('/:id',(req,res) =>{
    if(!ObjectId.isValid(req.params.id)){
        return res.status(400).send("no records present with the id")
    }
    Employee.findById(req.params.id,(err,docs) =>{
        if(!err){
            res.send(doc)
        }
        else{
            console.log("there is no one with the id"+ JSON.stringify(err,undefined,2))
        }
    }) 
})
router.post('/',(req,res) =>{
    var emp = new Employee({
        name : req.body.name,
        age: req.body.age,
        email: req.body.email,
        salary:req.body.salary
    })
    var transporter = nodemailer.createTransport({
        service: 'yahoo',
        auth: {
          user: 'krishna_neeraj@yahoo.com',
          pass: 'vrygjcckpukckxjz'
        }
      });
      
      var mailOptions = {
        from: 'krishna_neeraj@yahoo.com',
        to: req.body.email,
        subject: 'Response mail',
        text: 'Thank u for entering the details'
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    emp.save((err,doc) =>{
        if(!err){
            res.send(doc);
        }
        else{
            console.log("cannot add employee" +JSON.stringify(err,undefined,2))
            
        }

    })
})
router.put('/:id',(req,res) =>{
    if(!ObjectId.isValid(req.params.id)){
        return res.status(400).send("no records present with the id " )
    }
    var emp = {
        name :req.body.name,
        age: req.body.age,
        email: req.body.email,
        salary: req.body.salary
    }
    Employee.findByIdAndUpdate(req.params.id,{$set :emp},{new :true},(err,doc) =>{
        if(!err){
            res.send(doc)
        }
        else{
            console.log("Thers is no one with the id to update "+ JSON.stringify(err,undefined,2))
        }
    })
})
router.delete('/:id',(req,res) =>{
    if(!ObjectId.isValid(req.params.id)){
        return res.status(400).send("no records present with the id " )
    }
    Employee.findByIdAndDelete(req.params.id,(err,doc) =>{
        if(!err){
            res.send(doc)
        }
        else{
            console.log("Thers is no one with the id to delete "+ JSON.stringify(err,undefined,2))
        }
    })
})
module.exports= router;